<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'my_project');

// Project repository
set('repository', 'git@gitee.com:larlence/qihuo_php.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
set('shared_files', [

]);

set('shared_dirs', [
    'public/uploads'
]);

set('shared_files', [
    '.env'
]);
// Writable dirs by web server
set('writable_dirs', [
    'runtime'
]);

// Hosts
host('ttzq_test')
    ->stage('testing')
    ->set('branch', 'master')
    ->set('deploy_path', '/usr/local/nginx/html/qihuo_php');

//项目目录初始化
task('init_project', function () {

    //run("cp {{deploy_path}}/release/.env.testing {{deploy_path}}/release/.env");
});
after('deploy:vendors', 'init_project');


//部署完成重启PHP-FPM
task('reload:php-fpm', function () {
    run('sudo /etc/init.d/php-fpm restart');
});
after('cleanup', 'reload:php-fpm');

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');