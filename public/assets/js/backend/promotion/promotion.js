define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'promotion/promotion/index',
                    add_url: 'promotion/promotion/add',
                    edit_url: 'promotion/promotion/edit',
                    del_url: 'promotion/promotion/del',
                    multi_url: 'promotion/promotion/multi',
                    table: 'promotion',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                showToggle: false,
                showColumns: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title:'ID', sortable: true},
                        {field: 'name', title: '名称'},
                        {field: 'url', title: 'url'},
                        {field: 'pv', title: 'PV'},
                        {field: 'uv', title: 'UV'},
                        {field: 'status', title: '状态', searchList: {"1":'开启', "0": '关闭'}, formatter: Table.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});