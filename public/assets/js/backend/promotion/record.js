define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'promotion/record/index',
                    add_url: 'promotion/record/add',
                    edit_url: 'promotion/record/edit',
                    del_url: 'promotion/record/del',
                    multi_url: 'promotion/record/multi',
                    table: 'promotion',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                showToggle: false,
                showColumns: false,
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title:'ID', sortable: true, operate: false},
                        {
                            field: 'promotion.id',
                            title: '任务名称',
                            visible: false,
                            addclass: 'selectpage',
                            extend: 'data-source="promotion/promotion/index" data-field="name"',
                            operate: '=',
                            formatter: Table.api.formatter.search
                        },
                        {field: 'url', title: '链接', operate: false},
                        {field: 'platform', title: '平台'},
                        {field: 'device', title: '设备'},
                        {field: 'version', title: '版本'},
                        {field: 'browser', title: '浏览器'},
                        {field: 'ip', title: 'IP'},
                        {field: 'ua', title: 'UA'},
                        {
                            field: 'created_at',
                            title: '更新时间',
                            sortable: true,
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});