<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use app\common\library\Token;
use think\Request;

class Chart extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index(Request $request){
        $symbol = $request->param('symbol') ? $request->param('symbol'): 'SH000001';
        $period = $request->param('period') ? $request->param('period'): 'D';

        return $this->view->fetch('index', [
            'symbol' => $symbol,
            'period' => $period
        ]);
    }

    public function charts_query(){
        return $this->view->fetch();
    }

    //获取k线数据
    public function kms(Request $request)
    {
        $symbol=$request->param('symbol');
        $period=$request->param('period');
        $lasttick=(int)$_GET['lasttick'];
        if($lasttick>0) {
            echo \app\common\library\FutureService::getInstance()->GetLstKMaps($symbol, $period, $lasttick);
        } else {
            echo \app\common\library\FutureService::getInstance()->GetKMaps($symbol, $period);
        }
    }

}
