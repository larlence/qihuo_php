<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/9
 * Time: 15:08
 */

namespace app\index\controller;


use app\common\controller\Frontend;
use app\common\model\ArticleModel;
use app\common\model\Promotion;
use think\Request;

class Article extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function read(Request $request)
    {

        Promotion::doPromotion(1);

        $article_id = $request->request('article_id');
        try {
            $article = ArticleModel::where('id', $article_id)->find();
            if (empty($article)) {
                exception('未找到文章');
            }

            return $this->view->fetch('read', ['detail' => $article]);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }

    }
}