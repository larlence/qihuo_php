<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use app\common\model\Promotion;
use app\common\model\PromotionRecord;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        return $this->view->fetch();
    }

    public function privacy_policy()
    {
        return $this->view->fetch();
    }

    public function news()
    {
        $newslist = [];
        return jsonp(['newslist' => $newslist, 'new' => count($newslist), 'url' => 'https://www.fastadmin.net?ref=news']);
    }

    public function affirms()
    {
        Promotion::doPromotion(1);
        return $this->view->fetch();
    }
}
