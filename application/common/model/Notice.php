<?php

namespace app\common\model;

use think\Model;


class Notice extends Model
{
    CONST IS_OPEN = 0;
    CONST IS_DISABLE = 1;
}
