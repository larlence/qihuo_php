<?php

namespace app\common\model;

use think\Model;


class CommentModel extends Model
{
    CONST IS_OPEN = 0;
    CONST IS_DISABLE = 1;

    protected $table = 'fa_future_comment';

}
