<?php

namespace app\common\model;

use Jenssegers\Agent\Agent;
use think\Model;
use think\Request;

/**
 * 会员模型
 */
class PromotionRecord  Extends Model
{

    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = 'created_at';
    protected $updateTime = 'updated_at';
    // 追加属性
    protected $append = [

    ];

    public function promotion()
    {
        return $this->belongsTo('Promotion', 'promotion_id')->setEagerlyType(0);
    }

    public static function isVisitedToday($promotion_id)
    {
        return PromotionRecord::where([
            'promotion_id' => $promotion_id,
            'ip'        => Request::instance()->ip()
        ])->whereTime('created_at', 'today')->find();
    }
}
