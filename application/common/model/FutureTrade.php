<?php

namespace app\common\model;

use think\Model;


class FutureTrade extends Model
{
    CONST BUY = 1;
    CONST SELL = 2;

    const IS_LIQUIDATION = 1;
    const IS_NOT_LIQUIDATION = 0;
}
