<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/14
 * Time: 10:18
 */

namespace app\common\library;


class FutureService
{
    protected $appcode = '';

    static private $instance;

    private function __construct()
    {
        $this->appcode = '85f1afa36c024229a119bea354e09843';
    }

    private function __clone()
    {

    }

    public static function getInstance()
    {
        if (!self::$instance instanceof FutureService) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function GetLstKMaps($symbol, $period, $fromtick)
    {
        $url = "http://alirm-com.konpn.com/query/comlstkm?period=" . $period . "&rout=*&symbol=" . $symbol . "&fromtick=" . strval($fromtick);
        $cache_key = $symbol .'_'. $period;
        $cache = cache($cache_key);
        if (empty($cache)) {
            $data = $this->http_request($url, $this->appcode);
            cache($cache_key, $data, 86400);
            return $data;
        }
        return $cache;
    }

    public function getComrms($sysbols)
    {
        $url = "http://alirm-com.konpn.com/query/comrms?symbols=". $sysbols;
        $cache_key = $sysbols;
        $cache = cache($cache_key);
        if (empty($cache)) {
            $data = json_decode($this->http_request($url, $this->appcode));
            cache($cache_key, $data, 86400);
            return $data;
        }
        return $cache;
    }

    public function GetKMaps($symbol, $period)
    {
        $ckey = 'kmscache' . $symbol . $period;
        $cachekms =  cache($ckey);
        if(empty($cachekms) == true)
        {
            $cachekms = array();
            for($i=1; $i<=10; $i++)
            {
                $url = "http://alirm-com.konpn.com/query/comkm?period=" . $period . "&rout=*&pidx=" . strval($i) . "&symbol=".$symbol;
                $obj = json_decode($this->http_request($url,$this->appcode));
                if(empty($obj) == true || $obj->Code < 0){

                } else {
                    $cachekms=array_merge($cachekms, $obj->Obj);
                }
            }

            if(empty($cachekms) == true || count($cachekms) <= 0){

            } else {
                if($period=="1M"){
                    cache($ckey, $cachekms, 86400);
                } else if($period=="5M"){
                    cache($ckey, $cachekms, 86400);
                } else if($period=="15M"){
                    cache($ckey, $cachekms, 86400);
                } else if($period=="30M"){
                    cache($ckey, $cachekms, 86400);
                } else if($period=="1H"){
                    cache($ckey, $cachekms, 86400);
                } else if($period=="D"){
                    cache($ckey, $cachekms, 86400);
                } else {
                    cache($ckey, $cachekms, 86400);
                }
            }

            $retl["Code"] = 1;
            $retl["Obj"] = $cachekms;
            return json_encode($retl);
        }

        $retl["Code"] = 0;
        $retl["Obj"] = $cachekms;
        return json_encode($retl);
    }

    private function http_request($url)
    {
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $this->appcode);
        array_push($headers, "Accept:application/json");
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        if (1 == strpos("$".$url, "https://")) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        $result = curl_exec ($curl);
        curl_close($curl);
        return $result;
    }
}