<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/8
 * Time: 17:21
 */

namespace app\api\controller;


use app\common\controller\Api;
use think\Exception;
use think\Request;
use app\common\model\CommentModel;

class Comment extends Api
{
    protected $noNeedRight = ['*'];

    public function index()
    {
        $list = CommentModel::alias('c')->join('fa_user u', 'c.user_id = u.id')->field('u.nickname,u.avatar,c.content,c.created_at')->order('c.created_at', 'desc')->paginate();
        $this->success('', $list);
    }

    public function create(Request $request)
    {
        $content = $request->request('content');
        $future_id = $request->request('future_id');

        try {
            $future = \app\common\model\Future::find($future_id);
            if (empty($future)) {
                exception('期货类型不存在');
            }

            $data = [
                'user_id'       => $this->auth->id,
                'future_id'     => $future_id,
                'status'        => CommentModel::IS_OPEN,
                'content'       => $content,
                'created_at'    => time(),
            ];

            if (!CommentModel::create($data)) {
                exception('评论失败');
            };

            $this->success();
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
    }
}