<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/11/9
 * Time: 10:26
 */

namespace app\api\controller;


use app\common\controller\Api;
use app\common\model\ArticleModel;
use app\common\model\Category;
use think\Exception;
use think\process\exception\Timeout;
use think\Request;

class Article extends Api
{
    protected $noNeedLogin = ['*'];

    protected $noNeedRight = ['*'];

    public function index(Request $request)
    {
        try {
            $category_id = $request->request('category_id');
            $list = ArticleModel::alias('a')
                ->join('fa_category c', 'c.id = a.category_id')
                ->where(['a.category_id' => $category_id, 'a.status' => ArticleModel::IS_OPEN])
                ->field('a.id, a.title, a.desc, a.image, a.created_at')
                ->paginate();
            $this->success('', $list);
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
    }

    public function hot_list()
    {
        try {
            $category = Category::getCategoryArray('article');
            $list = [];
            if (!empty($category)) {
                foreach ($category as $key => $value) {
                    $list[$key]['category_id'] = $value['id'];
                    $list[$key]['category_name'] = $value['name'];
                    $list[$key]['articles'] = ArticleModel::where(['category_id' => $value['id'], 'status' => ArticleModel::IS_OPEN])->field('id, title, desc,image, created_at')->order('created_at', 'desc')->limit(3)->select();
                }
            }
            $future = \app\common\library\FutureService::getInstance()->getComrms('USDCNH,SH000001,SHFEAU,CMEJY');
            $this->success('', ['categories' => $list, 'banners' => ['assets/img/b1@3x.png', 'assets/img/b2@3x.png'], 'futures' => $future->Obj]);
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
    }


}