<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\Notice;

/**
 * 首页接口
 */
class Index extends Api
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 首页
     * 
     */
    public function index()
    {
        $notice = Notice::order('created_at', 'desc')->limit(3)->field('title')->select();
        $this->success('success', ['notice' => $notice, 'banners' => ['assets/img/b1@3x.png', 'assets/img/b2@3x.png']]);
    }


    public function future()
    {
        $result = \app\common\model\Future::where('status', \app\common\model\Future::IS_OPEN)->select();
        /*$future = \app\common\model\Future::where('status', \app\common\model\Future::IS_OPEN)->column('symbol');
        //拆分2次批量查询
        $count = count($future);
        if ($count > 10) {
            $data = [];
            $tmp = array_chunk($future, 10);
            foreach ($tmp as $item)
            {
                $res = \app\common\library\FutureService::getInstance()->getComrms(implode(',', $item));
                array_push($data, $res->Obj);
            }
            foreach ($data as $item) {
                foreach ($item as $value) {
                    $ret[] = $value;
                }
            }
        } else {
            $ret = \app\common\library\FutureService::getInstance()->getComrms();
        }*/
        foreach ($result as $key => $item) {
            $response = \app\common\library\FutureService::getInstance()->getComrms($item['symbol']);
            $item->data('info', $response->Obj[0]);
        }
        $this->success('success', $result);
    }

}
